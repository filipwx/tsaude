import Home from './components/home/Index.vue';
import How from './components/howto/Index.vue';
import Contact from './components/contact/Index.vue';

export const routes = [
    { path: '', component: Home, titulo: 'Home'},
    { path: '/como-funciona', component: How, titulo: 'Sobre'},
    { path: '/contato', component: Contact, titulo: 'Contato'},
    { path: '*', redirect: '/' }
];
