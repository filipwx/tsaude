// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
// import Vuetify from 'vuetify';
import { routes } from './routes';
Vue.config.productionTip = false
import Buefy from 'buefy'
// import 'vuetify/dist/vuetify.min.css'
import 'buefy/lib/buefy.css'

// Vue.use(Vuetify)
Vue.use(Buefy)
Vue.use(VueRouter);
Vue.use(VueAxios, axios)

/* eslint-disable no-new */

const router = new VueRouter({
  routes,
  mode: 'history'
});

var body = window.document.body;
var scroll = window;
var id = window.document.getElementById("navBar");
var myHeroClass = document.getElementById("navBar");
body.onscroll = function() {
  if (scroll.scrollY >= 120) {
    window.document.getElementById("navBar").style.background = "rgba(7, 194, 206, 0.64)";
    console.log(scroll.scrollY);
  } else {
    // id.classList.remove("nav");
    window.document.getElementById("navBar").style.background = "transparent";

  }
  console.log(scroll.scrollY);

}

new Vue({
  el: '#app',
  template: '<App/>',
  router,
  components: { App }
})
